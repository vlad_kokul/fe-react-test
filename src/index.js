import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Circles from './Circles';
import reportWebVitals from './reportWebVitals';
import AxisContainer from './AxisContainer';

ReactDOM.render(
    <React.StrictMode>
        <h1>Activity Time</h1>
        <Circles/>
        <AxisContainer/>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
