import React, {useState, useEffect} from 'react';
import './App.css';
import {useSpring, animated} from 'react-spring'
import moment from 'moment';

const generateDataset = () => (
    [
        {
            serial: 1,
            label: "camera #1",
            description: "its a camera 1",
            started_at: 1613393819000, // 14:56:59
            completed_at: 1613404619000, //  17:56:59
            duration: 5
        },
        {
            serial: 2,
            label: "camera #2",
            description: "its a camera 2",
            started_at: 1613429819000, // 0:56:59
            completed_at: 1613429819000, // 0:56:59
            duration: 5
        },
        {
            serial: 3,
            label: "camera #3",
            description: "its a camera 3",
            started_at: 1613432563000,
            completed_at: 1613432563000,
            duration: 5
        },
        {
            serial: 4,
            label: "camera #4",
            description: "its a camera 4",
            started_at: 1613429819000,
            completed_at: 1613429819000,
            duration: 5
        },
        {
            serial: 5,
            label: "camera #5",
            description: "its a camera 5",
            started_at: 1613349763000,
            completed_at: 1613349763000,
            duration: 5
        },
        {
            serial: 6,
            label: "camera #6",
            description: "its a camera6",
            started_at: 1613353363000,
            completed_at: 1613353363000,
            duration: 5
        },
        {
            serial: 7,
            label: "camera #7",
            description: "its a camera 7",
            started_at: 1613356963000,
            completed_at: 1613356963000,
            duration: 5
        },
        {
            serial: 6,
            label: "camera #6",
            description: "its a camera6",
            started_at: 1613353363000,
            completed_at: 1613353363000,
            duration: 5
        },
        {
            serial: 6,
            label: "camera #6",
            description: "its a camera6",
            started_at: 1613353363000,
            completed_at: 1613353363000,
            duration: 5
        },
        {
            serial: 8,
            label: "camera #8",
            description: "its a camera 8",
            started_at: 1613374963000,
            completed_at: 1613374963000,
            duration: 5
        },
        {
            serial: 9,
            label: "camera #9",
            description: "its a camera 9",
            started_at: 1613425363000,
            completed_at: 1613425363000,
            duration: 5
        },
    ]
);

const fetchData = (delay, value) => new Promise(resolve => setTimeout(resolve, delay, value));

const timeHelper =
    (item, timeFormat, shoulder) => moment(item[shoulder === 'end' ? 'completed_at' : 'started_at']).format(timeFormat);

const Circles = () => {
    const [visibleCircles, setVisibleCircles] = useState([]);
    const [dataMap, setDataMap] = useState({});

    useEffect(() => {
        const getData = async () => {
            const result = await fetchData(1000, generateDataset());
            let newDataset = {};
            result.forEach((item) => {
                let startedHH = timeHelper(item, 'H'),
                    endedHH = timeHelper(item, 'H', 'end'),
                    hoursLength = timeHelper(item, 'H', 'end') - timeHelper(item, 'H');

                newDataset[startedHH] = newDataset[startedHH] ? ++newDataset[startedHH] : 1;

                if (hoursLength) {
                    for (let i = 0; i < hoursLength; i++) {
                        newDataset[endedHH - i] = newDataset[endedHH - i] ? ++newDataset[endedHH - i] : 1;
                    }
                }
            });
            setDataMap(newDataset);
            setVisibleCircles(Object.keys(newDataset).map((item) => Number.parseInt(item)))
        };
        getData();
    }, []);
    return (
        <svg viewBox="0 0  100 4">
            {visibleCircles.map(d => (
                <Circle
                    key={d}
                    index={d}
                    radius={dataMap[d]}
                />
            ))}
        </svg>
    )
};

const Circle = ({index, radius}) => {
    const radiusRestriction = 4.5,
        r = radius ? radius * 1.5 : 0,
        style = useSpring({
            r: r > radiusRestriction ? radiusRestriction : r,
            opacity: 0.9,
            fill: "crimson"
        });
    return (
        <animated.circle {...style}
                         cx={index * 4.25 + 1}/>
    )
};

export default Circles;
