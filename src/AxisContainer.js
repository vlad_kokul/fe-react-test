import React from "react";

const ListItem = (props) => {
    return (<span>{props.item}</span>)
};

const Axis = (props) => {
    return (
        <div className="axis">
            {props.numbers.map((item) => <ListItem item={item} key={item.toString()}/>)}
        </div>)
};

const AxisContainer = () => {
    const dateNumbers = [12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    return (
        <>
            <div className="axis-container">
                <Axis numbers={dateNumbers}/>
                <Axis numbers={dateNumbers}/>
            </div>
            <div className="time-format">
                <span>AM</span>
                <span>PM</span>
            </div>
        </>
    )
};

export default AxisContainer;